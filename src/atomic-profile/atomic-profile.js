import { LitElement, html, css } from 'lit';
import sandbox from '../sandbox/sandbox';
import '../atomic-header/atomic-header.js';

class AtomicProfile extends LitElement {

    static get properties () {
        return {
            customer: {type:Object}
        };
    }

	static get styles()
	{
		return css `header{background-color: rgba(0, 0, 255, 0.63)};
        }`;
	}    

    constructor() {
        super();
        this.customer = null;
    }

    render() {
        if(!this.customer) return html``;
        console.log("Imangen del User esta en :" + this.customer.image.src);
        return html`
              <!-- CSS only -->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
              <!-- JavaScript Bundle with Popper -->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
 
            <header class="p-3 mb-3 border-bottom">
            <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">   
                <div class="col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0" >
                <h3 style="color:white;"> ATHOMIC</h3>
                <!--<a class="navbar-brand" href="">Registra tu compra. Escanea el Codigo</a>-->
                </div>
                    <div class="text-end">                      

                        <a href="#" class="d-block link-dark text-decoration-none">
                             <img @click=${this.infoProfile}  src="${this.customer.image.src}" alt="Usuario" width="40" height="40" class="rounded-circle" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ver Perfil">
                        </a> 
                    </div>
                    <div class="text-end">                      
                        <a href="#" class="d-block link-dark text-decoration-none">
                             <img @click=${this.exit}  src="./img/exit.png" alt="Salir" width="40" height="40" class="rounded-circle" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Cerrar Sesión">
                        </a> 
                    </div>
                </div>                            
            </div>    
    </header>
    `;
    }
	infoProfile()
	{
		console.log("Click para ver profile");
        console.log(this.customer);
        sandbox.dispatch('info-profile',{'cliente':this.customer},this);
	}

    exit() {

    }

}
customElements.define('atomic-profile', AtomicProfile)