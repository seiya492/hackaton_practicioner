import { LitElement, html, css } from 'lit-element';
import $ from '../ajax/ajax';
import sandbox from '../sandbox/sandbox';

class AtomicInput extends LitElement {
    static get properties() {
        return {
                src:{type:String},
                Usuarios:{type:Array},
                IdCom:{type:String}
                
        };
}
	
	constructor() {
		super();		
        this.src="";
		this.Usuarios = [];
        this.src="http://localhost:3000/Compras" //tomar url del componente main
        this.IdCom="";

        this.hide();
        this.obtenerSesion();
        sandbox.on('show-banner',this.show.bind(this));
	}
    
    static get styles()	{
        return css `
            .atomicinput{
                width: 86%;
                margin-top: 20px;
                margin-left: 7%;
                height: 80px;
            }
        `;
   	}
	
    render() {
        return html`	
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <aside class="atomicinput">
                <section class='pt-100'>
                    <div class="mt-100">
                        <label>Id Comercio</label>
                        <input type="text" .value="${this.IdCom}" @change="${this.updateIdCom}" input="${this.updateIdCom}" id="personFormName" class="form-control" placeholder="Id Comercio"/>								
                        <button class="w-1 btn bg-success" style="font-size: 10px" @click="${this.newCom}"><strong>+</strong></button>
                    </div>				
                </section>
            </aside>
            
        `;
    }
    
    show(){
        this.classList.remove("d-none");
    }   

    hide(){ 
        this.classList.add("d-none");
    }

    obtenerSesion(){
        if (sessionStorage.getItem("sesion")) {
            this.show();
        }
    }
  
    newCom() {
        $.ajax('GET',this.src+"?idCompra="+ this.IdCom,function(response)
        {
            this.Usuarios=response;
            console.log(this.Usuarios);

        }.bind(this));console.log("Identificador de Compra"+ this.IdCom);
    }
  
    updateIdCom(e){
        this.IdCom= e.target.value;
    }
}

customElements.define('atomic-input', AtomicInput)