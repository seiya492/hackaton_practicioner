import { LitElement, html } from 'lit-element';
import '../atomic-header/atomic-header.js';
import '../atomic-login/atomic-login.js';
import '../atomic-banner/atomic-banner.js';
import '../atomic-input/atomic-input.js';
import '../atomic-footer/atomic-footer.js';
import sandbox from '../sandbox/sandbox';
import ejecuteService from '../ajax/ajax';

class AtomicApp extends LitElement {

    static get properties() {
        return {
            users: { type: Array },
            url: { type: String }
        };
    }

    constructor() {
        super();
        this.users = [];
        this.url = 'http://localhost:3000/Usuarios';

        this.getUsers();
        this.obtenerSesion();
        sandbox.on('validate-login',this.validateLogin.bind(this));
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <atomic-header></atomic-header>
            <atomic-login></atomic-login>
            <atomic-banner></atomic-banner>
            <atomic-input></atomic-input> 
            <atomic-footer></atomic-footer>
        `;
    }

    getUsers(){
        ejecuteService.ajax('GET', this.url, function(response){
			this.users=response;
		}.bind(this));
    }

    obtenerSesion(){
        if (sessionStorage.getItem("sesion")) {
            let resultado  = JSON.parse(sessionStorage.getItem('user'));
            sandbox.dispatch('data-user',{'user': resultado},this);
            sandbox.dispatch('show-banner', {},this);
            
            /*sessionStorage.removeItem("sesion");
            sessionStorage.removeItem('user');*/
        }
    }

    validateLogin(e){
        if(!sessionStorage.getItem("sesion")){
            let resultado = this.users.find( user => user.nick === e.detail.user.nick &&  user.password === e.detail.user.passwd);
            if(resultado === undefined){
                sandbox.dispatch('show-login',{'user':this.user},this);
            }
            else{
                sandbox.dispatch('data-user',{'user': resultado},this);
                sandbox.dispatch('show-banner', {},this);
                sessionStorage.setItem('sesion', true);
                sessionStorage.setItem('user', JSON.stringify(resultado));
            }
        }
    }

}

customElements.define('atomic-app', AtomicApp)