import { LitElement, html } from 'lit-element';
import $ from '../ajax/ajax';
import sandbox from '../sandbox/sandbox';
import '../atomic-profile/atomic-profile.js';
import '../atomic-profiledetail/atomic-profileDetail.js';

class AtomicHeader extends LitElement {

    static get properties() {
        return {
            Usuario:{type: Object}
        };
    }

    constructor() {
        super();
        this.IdUser="1"; //Inicializar en vacio
        this.Usuario={};
        this.src="http://localhost:3000/Usuarios" //tomar url del componente main
        this.obtenerSesion();
        sandbox.on('data-user',this.obtenerSesion.bind(this));
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <nav class="navbar navbar-inverse fixed-top bg-light">
          <div class="container-fluid">
            <div class="navbar-header">
                <!--<a class="navbar-brand" href="">Registra tu compra. Escanea el Codigo</a>	
                <button class="w-1 btn bg-success" style="font-size: 10px" img ="banner.jpg" @click="${this.getUser}"><strong>Usuario</strong></button>
                <atomic-profile Usuario="${this.Usuario}"></atomic-profile>-->

                <atomic-profile .customer = "${this.Usuario}"></atomic-profile>
                <atomic-profiledetail class="d-none"></atomic-profiledetail>
            </div>

        </div>
        </nav>	
        `;
    }

    obtenerSesion(){
        if (sessionStorage.getItem("sesion")) {
            this.Usuario  = JSON.parse(sessionStorage.getItem('user'));
        }
    }

    getUser(e) {

        $.ajax('GET',this.src+"/"+ this.IdUser,function(response)
        {
            this.Usuario=response;
            console.log(this.Usuario);
    
        }.bind(this),this.Usuario);
        sandbox.dispatch('app-profile',{'InfoUsuario':this.Usuario},this);
    }    
}

customElements.define('atomic-header', AtomicHeader)