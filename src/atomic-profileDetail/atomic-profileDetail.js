import { LitElement, html, css } from 'lit-element';
import sandbox from '../sandbox/sandbox';

class atomicProfileDetail extends LitElement {
	
	static get properties() {
		return {			
			cliente: {type: Object}
		};
	}

	static get styles()
    {
         return css `:host{
                 position: absolute;
                 top: 400px;
                 left: 80%;
                 width:20%;
                 transform: translate(-50%, -50%);				 
                 z-index:10000;
                 }`;
    }
	
	constructor() {

		super();
		this.reset();
				
		sandbox.on('info-profile',this.mostrarInfo.bind(this));

	}
	
  render() {
    return html`	
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<div class='card'>
			<div class="card-header d-flex justify-content-between align-items-center">
				<h3>Perfil</h3>
				<button type="button" class="btn-close" aria-label="Cerrar" @click=${this.close} ></button>
	  		</div>
			<img  class="card-img-top rounded-circle" src="${this.cliente.image.src}" alt="Usuario" width="100" height="250">
			
			<div class='border m-2 p-2'>
				<form>
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" id="profileFormName"  class="form-control" placeholder="Nombre" .value="${this.cliente.name}" @input=${this.updateName} disabled/>
					</div>
					<div class="form-group">
						<label>NickName</label>
						<input type="text" id="profileFormNick"  class="form-control" placeholder="Nickname" .value="${this.cliente.nick}" @input=${this.updateNick} disabled/>
					</div>                  
					<div class="form-group">
						<label>RFC</label>
						<input type="text" id="profileFormRFC"  class="form-control" placeholder="RFC" .value="${this.cliente.rfc}" @input=${this.updaterrfc} disabled/>
					</div>                  
					<div class="form-group">
						<label>Teléfono</label>
						<input type="text" id="profileFormPhone"  class="form-control" placeholder="Phone" .value="${this.cliente.phone}" @input=${this.updaterPhone} disabled/>
					</div>                                        
					<div class="form-group">
						<label>Correo Electrónico</label>
						<input type="text" id="profileFormEmail"  class="form-control" placeholder="Email" .value="${this.cliente.mail}" @input=${this.updaterEmail} disabled/>
					</div>                  
					<div class="form-group">
						<label>Ciudad</label>
						<input type="text" id="profileFormEmail"  class="form-control" placeholder="Ciudad" .value="${this.cliente.ciudad}" @input=${this.updaterCiudad} disabled/>
					</div>                  
					<div class="form-group">
						<label>Estado</label>
						<input type="text" id="profileFormEmail"  class="form-control" placeholder="Estado" .value="${this.cliente.estado}" @input=${this.updaterEstado} disabled/>
					</div>                  										
				</form>
  			</div>
		</div>
	`;
  }

  reset()
  {
      this.cliente ={
		"id": "",
        "name": "",
        "nick": "",
        "nCliente": "",
        "rfc": "",
        "phone": "",
        "mail": "",
        "image": {
          "src": "../img/persona.jpg"
        },
        "ciudad": "",
        "estado": "",
        "comercio": false     
      };

  }
  show()
  {
	console.log("show()");
  	this.classList.remove("d-none");
  }   

  hide()
  {
	this.classList.add("d-none");
  }

 
  mostrarInfo(e)
  {
	this.classList.remove("d-none");
	console.log("mostrarInfo");
	this.cliente = e.detail.cliente;
	console.log(this.cliente);
	console.log(this.classList);
	this.classList.toggle("d-done");
	console.log(this.classList);
/*	if(this.classList.contains("d-done")){
		console.log("mostrar");
		this.show();	
	}else {
		console.log("ocultar");
		this.hide();	
	}
*/	
  }

  close(e) {
	  e.preventDefault();	  
      this.hide();
  }

}

customElements.define('atomic-profiledetail', atomicProfileDetail)
