import { LitElement, html, css } from 'lit-element';
import sandbox from '../sandbox/sandbox';

class AtomicBanner extends LitElement {

    static get properties() {
        return {
            texto: {type: String}
        };
    }
    
    static get styles()	{
        return css `
            .banner{
                width: 90%;
                margin-top: 100px;
                margin-left: 5%;
                height: 80px;
                border-radius: 50px 50px 50px 50px;
                -moz-border-radius: 50px 50px 50px 50px;
                -webkit-border-radius: 50px 50px 50px 50px;
                background-color: rgba(0, 0, 255, 0.63);
                text-align: center;
            }

            .banner h3{
                padding-top: 20px;
            }
        `;
   	}

    constructor() {
        super();
        this.texto = 'Registra tu compra escanea el código'
        
        this.hide();
        this.obtenerSesion();
        sandbox.on('show-banner',this.show.bind(this));
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div class="banner text-white">
                <h3>${this.texto}</h3>
            </div>
        `;
    }

    obtenerSesion(){
        if (sessionStorage.getItem("sesion")) {
            this.show();
        }
    }
    
    show(){
        this.classList.remove("d-none");
    }   

    hide(){ 
        this.classList.add("d-none");
    }

}

customElements.define('atomic-banner', AtomicBanner)