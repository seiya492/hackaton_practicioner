import { LitElement, html } from 'lit-element';

class AtomicFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <footer class="bg-blue text-left text-red fixed-bottom">
                <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                    ------La Vida Sin Papel-----
                </div>
                <div class="text-left p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                    @Esau Farias Bahena @Juan Carlos Villalpando @Rafael Corona Lopez
                    @Ricardo Bernardino Muñoz @Diana Espinoza Jimenez
                </div>
            </footer>			
        `;
    }
}

customElements.define('atomic-footer', AtomicFooter)