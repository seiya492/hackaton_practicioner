import { LitElement, html, css } from 'lit-element';
import sandbox from '../sandbox/sandbox';

class AtomicLogin extends LitElement {

    static get properties() {
        return {
            title: {type: String},
            btnLogin: {type: String},
            user: {type: Object}
        };
    }
    
    static get styles()	{
        return css `
            :host{
                position: absolute;
	            top: 300px;
        	    left: 50%;
                width: 450px;
	            transform: translate(-50%, -50%);
			    z-index:10000;
            }

            .modal-background{
                border-radius: 50px 50px 50px 50px;
                -moz-border-radius: 50px 50px 50px 50px;
                -webkit-border-radius: 50px 50px 50px 50px;
                background-color: rgba(0, 0, 255, 0.63);
            }

            .title{
                text-align: center;
                font-weight: 500;
                line-height: 1.2;
                width: 100%;
                font-size: x-large;
            }
            
            .img{
                width:  100px;
                height: 100px;
            }

            .centerImg{
                display: flex;
                justify-content: center;
                margin-bottom: 20px;
            }

            .centerBtn{
                margin-top: 30px;
                display: flex;
                justify-content: center;
            }
        `;
   	}

    constructor() {
        super();
        this.title='Bienvenido a Athomic';
		this.btnLogin='Iniciar Sesión';

        this.reset();
        this.obtenerSesion();
        sandbox.on('show-login',this.show.bind(this));
        sandbox.on('hide-login',this.hide.bind(this));
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div class='border border-secondary modal-background' id='formulario'>
                <div class='m-2 p-2'>
                    <form>
                        <div class="form-group">
                            <div class="">
                                <p class="title text-white">${this.title}</p>
                            </div>
                            <div class="centerImg">
                                <img src="./img/login.png" alt="iniciosesion" class="img-circle img">
                            </div>
                            <div class="form-group">
                                <label class="text-white">Usuario</label>
                                <input type="text" @input="${this.updateName}" id="userFormName" class="form-control" placeholder="Usuario"/>
                            </div>
                            <div class="form-group">
                            <label class="text-white">Contraseña</label>
                                <input type="password" @input="${this.updatePasswd}" id="userFormPasswd" class="form-control" placeholder="Contraseña"></input>
                            </div>
                            <div class="form-group centerBtn">
                                <button type="submit" @click=${this.login} class="btn btn-success"><strong>${this.btnLogin}</strong></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        `;
    }

    show(){
        this.classList.remove("d-none");
    }   

    hide(){ 
        console.log("me escondo");
        this.classList.add("d-none");
    }

    reset(){
        this.user = {
            "nick" : "",
            "passwd": ""
        };
    }

    obtenerSesion(){
        if (sessionStorage.getItem("sesion")) {
            this.hide();
        }
    }

    updateName(e) {
        this.user.nick = e.target.value;
    }

    updatePasswd(e) {
        this.user.passwd = e.target.value;
    }

    login(e){
        e.preventDefault();
        this.hide();
        sandbox.dispatch('validate-login',{'user':this.user},this);
    }

}

customElements.define('atomic-login', AtomicLogin)